package com.example.atop;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    Button especifico, general;
    ImageButton imageButton;
    EditText titulo, descripcion;
    String token ="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        especifico = (Button) findViewById(R.id.especifico);
        general = (Button) findViewById(R.id.general);
        titulo = (EditText) findViewById(R.id.titulo);
        descripcion = (EditText) findViewById(R.id.descripcion);

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.i("tag", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();
                    }
                });
        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(MainActivity.this,"Se agrego a la lista", Toast.LENGTH_LONG).show();
            }
        });

        especifico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llamarEspecifico();
            }
        });
        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llamarGeneral();
            }
        });
    }


    private void llamarEspecifico() {
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try{
            json.put("to",token);
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", titulo.getText());
            notificacion.put("detalle", descripcion.getText());
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, json,null,null){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAAaSKeTf4:APA91bEe4X4_myEbeylbvrDvbadYt-9MdaP1Q50Mi596spDnfilayUv2ctfZIwOZeNOy-gd9qNFHhXpRQZFZyZKXGcobGaoC6rRzRd8H6WBktqAHBpvxj-W0voEP-4f6mviA6Fouovkl");
                    return header;
                }
            };
            myrequest.add(request);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void llamarGeneral() {
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try{
            json.put("to","/topics/"+"atodos");
            JSONObject notificacion = new JSONObject();
            notificacion.put("titulo", titulo.getText());
            notificacion.put("detalle", descripcion.getText());
            json.put("data", notificacion);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, json,null,null){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type", "application/json");
                    header.put("authorization", "key=AAAAaSKeTf4:APA91bEe4X4_myEbeylbvrDvbadYt-9MdaP1Q50Mi596spDnfilayUv2ctfZIwOZeNOy-gd9qNFHhXpRQZFZyZKXGcobGaoC6rRzRd8H6WBktqAHBpvxj-W0voEP-4f6mviA6Fouovkl");
                    return header;
                }
            };
            myrequest.add(request);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
